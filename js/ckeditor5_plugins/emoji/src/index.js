/**
 * @file The build process always expects an index.js file. Anything exported
 * here will be recognized by CKEditor 5 as an available plugin. Multiple
 * plugins can be exported in this one file.
 *
 * I.e. this file's purpose is to make plugin(s) discoverable.
 */

import Emoji from './emoji';
import EmojiActivity from './emoji-activity';
import EmojiFlags from './emoji-flags';
import EmojiFood from './emoji-food';
import EmojiNature from './emoji-nature';
import EmojiObjects from './emoji-objects';
import EmojiPeople from './emoji-people';
import EmojiPlaces from './emoji-places';
import EmojiSymbols from './emoji-symbols';

export default {
    Emoji,
    EmojiActivity,
    EmojiFlags,
    EmojiFood,
    EmojiNature,
    EmojiObjects,
    EmojiPeople,
    EmojiPlaces,
    EmojiSymbols,
};
